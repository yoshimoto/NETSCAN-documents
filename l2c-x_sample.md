# 3plate分、0peke

PL1 PL2 PL3 (posで10 20 30) の3枚分、つまりPL1-PL2 PL2-PL3の2セット分のリンクレット情報を使って、飛跡がないのを許容しない=0pekeで接続する

## chain.rc

```
[Chain]
UsePos = 3 10 20 30
PmPeke = 0
Format = 0
```

使うPosは3つ、10と20と30。
PmPekeは今回は0、つまり隣接間で繋がっていることを要求。
Formatは0以外使えない。


## linklet-dump.lst

```
linklet-dump-001-002.lst
linklet-dump-002-003.lst
```
2ファイルを入力する

## linklet-dump-001-002.lst

```
10 0 20 0
```
pos=10のrawid=0とpos=20のrawid=0が繋がっているという情報

## linklet-dump-002-003.lst

```
20 0 30 0
20 0 30 1
```
pos=20のrawid=0とpos=30のrawid=0 又は rawid=1が繋がっているという情報

```mermaid
graph LR;
pos=10:rawid=0-->pos=20:rawid=0
pos=20:rawid=0-->pos=30:rawid=0
pos=20:rawid=0-->pos=30:rawid=1
```

## 実行するコマンド
```
l2c-x.exe --rc chain.rc --o chain-output.lst @linklet-dump.lst
```

## chain-output.lst
```
#	N_pos = 3
#	UsePos = 10 20 30
#	PmPeke = 0
0	2	1	3	4	-1	-1	-1	-1
	0	-1	-1	1	3	3	0	0
		0	0	1
	1	-1	-1	1	3	3	0	0
		0	0	0
#	N_chain = 2
#	N_group = 1
#	N_linklet = 3
```

可読性がないので、パーサーを使って本体部分を書き換える。


```yaml
- GroupID: 0
  NChains: 2
  BeginPlate: 1
  EndPlate: 3
  NSegments: 4
  NRoots: -1
  NLeafs: -1
  MeanNSegments: -1
  SigmaNSegments: -1
  Chains:
  - ChainID: 0
    LeafID: -1
    RootID: -1
    BeginPlate: 1
    EndPlate: 3
    NSegments: 3
    Peke1: 0
    Peke2: 0
    Segments:
    - 0
    - 0
    - 1
  - ChainID: 1
    LeafID: -1
    RootID: -1
    BeginPlate: 1
    EndPlate: 3
    NSegments: 3
    Peke1: 0
    Peke2: 0
    Segments:
    - 0
    - 0
    - 0
```

とりあえず読める形式になった。

# 3plate分、1peke

飛跡があるのを1個だけ許容する=1pekeをしたい。なので、PL1-PL2 PL2-PL3 及び PL1-PL3 の3セット分のリンクレット情報を使って接続する。

まず、PmPekeを1にする

## chain.rc

```
[Chain]
UsePos = 3 10 20 30
PmPeke = 1
Format = 0
```
## linklet-dump-001-003.lst
PL1-PL3のリンクレット情報を用意する

```
10 0 30 5
```
pos=10のrawid=0とpos=30のrawid=5が繋がっているという情報


```mermaid
graph LR;
pos=10:rawid=0-->pos=20:rawid=0
pos=20:rawid=0-->pos=30:rawid=0
pos=20:rawid=0-->pos=30:rawid=1
pos=10:rawid=0-->peke
peke-->pos=30:rawid=5
```


3つ目のリンクレット情報をlinklet-dump.lstに入れる

## linklet-dump.lst

```
linklet-dump-001-002.lst
linklet-dump-002-003.lst
linklet-dump-001-003.lst
```


## 実行するコマンド
```
l2c-x.exe --rc chain.rc --o chain-output.lst @linklet-dump.lst --output-isolated-linklet
```

`--output-isolated-linklet` を追加した

はい、結果発表

```
#	N_pos = 3
#	UsePos = 10 20 30
#	PmPeke = 1
0	3	1	3	5	-1	-1	-1	-1
	0	-1	-1	1	3	2	1	1
		0	-1	5
	1	-1	-1	1	3	3	0	0
		0	0	1
	2	-1	-1	1	3	3	0	0
		0	0	0
#	N_chain = 3
#	N_group = 1
#	N_linklet = 4
```

```yaml
- GroupID: 0
  NChains: 2
  BeginPlate: 1
  EndPlate: 3
  NSegments: 4
  NRoots: -1
  NLeafs: -1
  MeanNSegments: -1
  SigmaNSegments: -1
  Chains:
  - ChainID: 0
    LeafID: -1
    RootID: -1
    BeginPlate: 1
    EndPlate: 3
    NSegments: 3
    Peke1: 0
    Peke2: 0
    Segments:
    - 0
    - 0
    - 1
  - ChainID: 1
    LeafID: -1
    RootID: -1
    BeginPlate: 1
    EndPlate: 3
    NSegments: 3
    Peke1: 0
    Peke2: 0
    Segments:
    - 0
    - 0
    - 0
```

1pekeのChainが出てこない。なぜ?

# ケース 分岐1

次のようなケースを考える。

```mermaid
graph LR;
pos=10:rawid=0-->pos=20:rawid=0
pos=10:rawid=0-->pos=20:rawid=1
pos=20:rawid=1-->pos=30:rawid=0
pos=20:rawid=0-->pos=30:rawid=0
```

```yaml
- GroupID: 0
  NChains: 2
  BeginPlate: 1
  EndPlate: 3
  NSegments: 4
  Chains:
  - ChainID: 0
    BeginPlate: 1
    EndPlate: 3
    NSegments: 3
    Peke1: 0
    Peke2: 0
    Segments:
    - 0
    - 1
    - 0
  - ChainID: 1
    BeginPlate: 1
    EndPlate: 3
    NSegments: 3
    Peke1: 0
    Peke2: 0
    Segments:
    - 0
    - 0
    - 0

```

# ケース 分岐2

次のようなケースを考える。

```mermaid
graph LR;
pos=10:rawid=0-->pos=20:rawid=0
pos=10:rawid=1-->pos=20:rawid=0
pos=20:rawid=0-->pos=30:rawid=0
pos=20:rawid=0-->pos=30:rawid=1
```

```yaml
- GroupID: 0
  NChains: 4
  BeginPlate: 1
  EndPlate: 3
  NSegments: 5
  Chains:
  - ChainID: 0
    BeginPlate: 1
    EndPlate: 3
    NSegments: 3
    Peke1: 0
    Peke2: 0
    Segments:
    - 1
    - 0
    - 1
  - ChainID: 1
    BeginPlate: 1
    EndPlate: 3
    NSegments: 3
    Peke1: 0
    Peke2: 0
    Segments:
    - 1
    - 0
    - 0
  - ChainID: 2
    BeginPlate: 1
    EndPlate: 3
    NSegments: 3
    Peke1: 0
    Peke2: 0
    Segments:
    - 0
    - 0
    - 1
  - ChainID: 3
    BeginPlate: 1
    EndPlate: 3
    NSegments: 3
    Peke1: 0
    Peke2: 0
    Segments:
    - 0
    - 0
    - 0
```

