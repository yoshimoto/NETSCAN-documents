---
#### filter list
---

+ list-filter-file format for base tracks
  - specify each base track case-1
  > pl rawid  

  - specify each base track case-2
  > pos0 col0 row0 zone0 isg0 pos1 col1 row1 zone1 isg1  
  >     i.e. micro-track-id in face 0 and 1  

  - specify all base tracsk in a view
  > pos col row zone  

+ list-filter-file format for micro tracks
  - specify each micro track case-1
  > pos rawid  

  - specify each micro track case-2
  > pos col row zone isg  

  - specify all micro tracsk in a view
  > pos col row zone  
