---
#### cpic_rel - make plots of relative corrmap - 32bit only
---

+ usage : cpic_rel fname-corrmap fname-ps [ options ]
  
  > fname-corrmap : relative correction map to be plotted.  
  > fname-ps      : output ps file name.  
  
+ options

  - --view-id
  > show view-id in figures  
  
  - --scale scale-in-position scale-in-angle
  > set relative scale to their default values of 50micron and 50mrad  
