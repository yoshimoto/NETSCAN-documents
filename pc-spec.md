---
#### netscan のシステム要件 2015年10月更新時点
---
```
OS:
  必須: Windows 7SP1/8以降 x86/x64, 
        Windows Server 2008 R2 SP1以降 x64
  推奨: Windows 7SP1/8.1 x64, 10(Pro版以上) x64
  
    注1: Windows 8 無印 は2016年1月にサポートが切れます
    注2: Windows 10 Home は推奨しません

CPU:
  必須: Intel Core i3/i5/i7 第2世代以降,
        Intel Xeon E3/E5/E7 第2世代以降,
        AMD FX-4xxx/6xxx/8xxx,
        AVXに対応したAMD APU
  推奨: Haswell-E/EP (Core i7-58xx/59xx等),
        Broadwell-K (Core i7-57xx),
        Skylake-S (Core i7-67xx)

メモリー:
  必須: 4GB
  推奨: 16GB以上 かつ 物理CPUコア数×4GB以上
```

+ CPU 世代判別
  > Corei#-N* ( N が世代番号 )  
