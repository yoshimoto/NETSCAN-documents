---
#### mk_corrmap - make relative corrmap between plates -
---

+ usage : mk_corrmap corrmap-rel-input corrmap-rel-output event-descriptor pos_1 pos_2 ... pos_n

  > integrate relative corrmap in corrmap-rel-input from pos_1 to pos_n  
  > and output relative corrmap between pos_1 and pos_n  
